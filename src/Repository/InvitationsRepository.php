<?php

namespace App\Repository;

use App\Entity\Invitations;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Invitations|null find($id, $lockMode = null, $lockVersion = null)
 * @method Invitations|null findOneBy(array $criteria, array $orderBy = null)
 * @method Invitations[]    findAll()
 * @method Invitations[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InvitationsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Invitations::class);
    }

//    /**
//     * @return Invitations[] Returns an array of Invitations objects
//     */
    
    public function findBySender($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.id_sender = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(50)
            ->getQuery()
            ->getArrayResult()
        ;
    }
    
    public function findByReceiver($value)
    {
        return $this->createQueryBuilder('i')
        ->andWhere('i.id_receiver = :val')
        ->setParameter('val', $value)
        ->orderBy('i.id', 'ASC')
        ->setMaxResults(50)
        ->getQuery()
        ->getArrayResult()
        ;
    }
    
    public function findOneByStatus($value): ?Invitations
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.status_action = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getArrayResult()
        ;
    }
    
}
