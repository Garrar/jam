<?php
namespace App\Controller;

use App\Entity\Invitations;

class InvitationsController extends JamApiController
{
    public function __construct(){
        parent::__construct();
    }
    
    public function send()
    {
        if (!parent::checkMandatory(['subject','sender','receiver','start_date','location'])) {
            return parent::output();
        }
        
        $entityManager = $this->getDoctrine()->getManager();
        
        $sDate = \DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s'));
        $sStartDate = parent::getParam('start_date' , date('Y-m-d H:i:s'));
        $sEndDate = parent::getParam('end_date', FALSE);
        
        $oStartDate = \DateTime::createFromFormat('Y-m-d H:i:s', $sStartDate);
        
        $oEndDate = (!$sEndDate || (strtotime($sStartDate) > strtotime($sEndDate))) ? \DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s', strtotime($sStartDate) + 60*60)) : \DateTime::createFromFormat('Y-m-d H:i:s', $sEndDate);
        
        $invitation = new Invitations();
        $invitation->setFirstname(parent::getParam('firstname'));
        $invitation->setLastname(parent::getParam('lastname'));
        $invitation->setSubject(parent::getParam('subject'));
        $invitation->setDescription(parent::getParam('description'));
        $invitation->setIdReceiver(parent::getParam('receiver'));
        $invitation->setIdSender(parent::getParam('sender'));
        $invitation->setLocation(parent::getParam('location'));
        $invitation->setCreateDate($sDate);
        $invitation->setLastModify($sDate);
        $invitation->setStartDate($oStartDate);
        $invitation->setEndDate($oEndDate);
        $invitation->setStatusAction('pending');
        
        // tell Doctrine to (eventually) save the Invitation (no queries yet)
        $entityManager->persist($invitation);
        
        // INSERT query
        $entityManager->flush();
        
        parent::error(200);
        parent::status(TRUE);
        parent::msg('The invitation sent correctly');
        parent::result(['id'=>$invitation->getId()
            ,'subject'=>$invitation->getSubject()
            ,'description'=>$invitation->getDescription()
            ,'sender'=>$invitation->getIdSender()
            ,'receiver'=>$invitation->getIdReceiver()
            ,'location'=>$invitation->getLocation()
            ,'start_date'=>$invitation->getStartDate()
            ,'end_date'=>$invitation->getEndDate()
            ,'create_date'=>$invitation->getCreateDate()
            ,'last_modify'=>$invitation->getLastModify()
            ,'status'=>$invitation->getStatusAction()
        ]);
        return parent::output();
    }
    
    public function accept()
    {
        if (!parent::checkMandatory(['id','receiver'])) {
            return parent::output();
        }
        
        $iInvitationID = parent::getParam('id',FALSE);
        if(!$this->valid($iInvitationID)) {
            return parent::output();
        }
        
        $aItem = $this->find($iInvitationID);
        
        if ($aItem->getIdReceiver() != parent::getParam('receiver',FALSE)) {
            parent::error(403);
            parent::status(FALSE);
            parent::msg(parent::errorMsg(parent::error()));
            return parent::output();
        }
        
        if ($aItem->getStatusAction() == 'deleted' || $aItem->getStatusAction() == 'canceled') {
            parent::error(200);
            parent::status(FALSE);
            parent::msg("The invitation was not accepted because the current status is ".$aItem->getStatusAction() );
            parent::result(['id'=>$iInvitationID,'status'=>$aItem->getStatusAction()]);
            return parent::output();
        }
        
        //EXE Function
        $this->setStatus($iInvitationID, 'accepted');
        
        parent::error(200);
        parent::status(TRUE);
        parent::msg('The invitation was accepted correctly');
        return parent::output();
    }
    
    private function setStatus($iInvitationID, $sStatus)
    {
        //EXE Func
        $item = $this->find($iInvitationID);
        $entityManager = $this->getDoctrine()->getManager();
        $item->setStatusAction($sStatus);
        $entityManager->flush();
    }
    
    
    public function decline()
    {
        if (!parent::checkMandatory(['id','receiver'])) {
            return parent::output();
        }
        
        $iInvitationID = parent::getParam('id',FALSE);
        if(!$this->valid($iInvitationID)) {
            return parent::output();
        }
        
        $aItem = $this->find($iInvitationID);
        
        if ($aItem->getIdReceiver() != parent::getParam('receiver',FALSE)) {
            parent::error(403);
            parent::status(FALSE);
            parent::msg(parent::errorMsg(parent::error()));
            return parent::output();
        }
        
        if ($aItem->getStatusAction() == 'deleted' || $aItem->getStatusAction() == 'canceled') {
            parent::error(200);
            parent::status(FALSE);
            parent::msg("The invitation was not declined because the current status is ".$aItem->getStatusAction() );
            parent::result(['id'=>$iInvitationID,'status'=>$aItem->getStatusAction()]);
            return parent::output();
        }
        
        //EXE Func
        $this->setStatus($iInvitationID, 'declined');
        
        parent::error(200);
        parent::status(TRUE);
        parent::msg('The invitation was declined correctly');
        parent::result(null);
        return parent::output();
    }
    
    public function cancel()
    {
        if (!parent::checkMandatory(['id'])) {
            return parent::output();
        }
        
        $iInvitationID = parent::getParam('id',FALSE);
        if(!$this->valid($iInvitationID)) {
            return parent::output();
        }
        
        //EXE Func
        $this->setStatus($iInvitationID, 'canceled');
        
        parent::error(200);
        parent::status(TRUE);
        parent::msg('The invitation was canceled correctly');
        parent::result(null);
        return parent::output();
    }
    
    public function delete()
    {
        if (!parent::checkMandatory(['id'])) {
            return parent::output();
        }
        
        $iInvitationID = parent::getParam('id',FALSE);
        if(!$this->valid($iInvitationID)) {
            return parent::output();
        }
        
        //EXE Func
        $this->setStatus($iInvitationID, 'deleted');
        
        parent::error(200);
        parent::status(TRUE);
        parent::msg('The invitation was deleted correctly');
        parent::result(null);
        return parent::output();
    }
    
    private function valid($iInvitationID)
    {
        if (!$iInvitationID) {
            parent::error(100);
            parent::status(TRUE);
            parent::msg(parent::errorMsg(parent::error()));
            parent::result(['id']);
            return FALSE;
        }
        
        if (!$item = $this->find($iInvitationID)) {
            parent::error(404);
            parent::status(FALSE);
            parent::msg('We have no invitation with this id');
            parent::result(['id'=>$iInvitationID]);
            return FALSE;
        }
        return TRUE;
    }
    
    private function find($iID)
    {
        return $this->getDoctrine()
        ->getRepository(Invitations::class)
        ->find($iID);
    }
    
    public function getItem()
    {
        if (!parent::checkMandatory(['id'])) {
            return parent::output();
        }
        
        $iInvitationID = parent::getParam('id',FALSE);
        if(!$this->valid($iInvitationID)) {
            return parent::output();
        }
        
        $aParams = ['id'=>$iInvitationID];
        
        $result = $this->getDoctrine()
        ->getRepository(Invitations::class)
        ->findBy($aParams);
        
        if(!$result) {
            parent::error(200);
            parent::status(FALSE);
            parent::msg('No invitations found');
            return parent::output();
        } else {
            $aOutput = [];
            foreach ($result as $invitation) {
                $aOutput = ['id'=>$invitation->getId()
                    ,'subject'=>$invitation->getSubject()
                    ,'sender'=>$invitation->getIdSender()
                    ,'receiver'=>$invitation->getIdReceiver()
                    ,'location'=>$invitation->getLocation()
                    ,'description'=>$invitation->getDescription()
                    ,'start_date'=>$invitation->getStartDate()
                    ,'end_date'=>$invitation->getEndDate()
                    ,'status'=>$invitation->getStatusAction()
                    ,'create_date'=>$invitation->getCreateDate()
                    ,'last_modify'=>$invitation->getLastModify()
                ];
            }
            parent::error(200);
            parent::status(TRUE);
            parent::msg('The invitation details');
            parent::total(count($result));
            parent::result($aOutput);
            return parent::output();
        }
    }
    
    public function list()
    {
        $aParams = ['id_sender'=>parent::getParam('sender',FALSE)
            ,'id_receiver'=>parent::getParam('receiver',FALSE)
            ,'status_action'=>parent::getParam('status',FALSE)
        ];
        
        foreach ($aParams as $iKey => $mValue) {
            if (!$mValue) unset($aParams[$iKey]);
        }
        
        $result = $this->getDoctrine()
        ->getRepository(Invitations::class)
        ->findBy($aParams);
        
        if(!$result) {
            parent::error(200);
            parent::status(FALSE);
            parent::msg('No invitations found');
            return parent::output();
        } else {
            $aOutput = [];
            foreach ($result as $invitation) {
                $aOutput[] = ['id'=>$invitation->getId()
                    ,'subject'=>$invitation->getSubject()
                    ,'sender'=>$invitation->getIdSender()
                    ,'receiver'=>$invitation->getIdReceiver()
                    ,'location'=>$invitation->getLocation()
                    ,'description'=>$invitation->getDescription()
                    ,'start_date'=>$invitation->getStartDate()
                    ,'end_date'=>$invitation->getEndDate()
                    ,'status'=>$invitation->getStatusAction()
                    ,'create_date'=>$invitation->getCreateDate()
                    ,'last_modify'=>$invitation->getLastModify()
                ];
            }
            parent::error(200);
            parent::status(TRUE);
            parent::msg('The invitations');
            parent::total(count($result));
            parent::result($aOutput);
            return parent::output();
        }
    }
}