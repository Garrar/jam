<?php
// src/Controller/JamController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use phpDocumentor\Reflection\Types\Null_;

class JamApiController extends Controller
{
    private $aApiResponse = [];
    public function __construct(){
        $this->aApiResponse = [];
    }
    
    public function getParam($sName, $mDefault=null)
    {
        return (isset($_REQUEST[$sName])) ? trim($_REQUEST[$sName]) : ((!is_null($mDefault)) ? $mDefault : false);
    }
    
    public function checkMandatory($aParams)
    {
        $bOk = TRUE;
        $aMissingParams = [];
        foreach ($aParams as $sValue) {
            if (!$this->getParam($sValue)) {
                $aMissingParams[] = $sValue;
                $bOk = FALSE;
            }
        }
        
        if($bOk) return TRUE;
        
        $this->error(100);
        $this->status(TRUE);
        $this->msg($this->errorMsg($this->error()));
        $this->result($aMissingParams);
        return FALSE;
    }
    
    public function errorMsg($iErrorCode, $bTranslate=FALSE)
    {
        $aError[200] = ['key'=>'success', 'default'=>'Success'];
        $aError[100] = ['key'=>'missing_mandatory_param', 'default'=>'Missing mandatory parameters'];
        $aError[101] = ['key'=>'noResults', 'default'=>'No results'];
        $aError[102] = ['key'=>'error_validation_param', 'default'=>'Error validation parameters'];
        $aError[500] = ['key'=>'bad_request', 'default'=>'Bad request'];
        $aError[403] = ['key'=>'access_denied', 'default'=>'Access denied'];
        $aError[198] = ['key'=>'error_db', 'default'=>'DB error'];
        $aError[199] = ['key'=>'error_writing_file', 'default'=>'Error'];
        $aError[201] = ['key'=>'already_exist', 'default'=>'Already exist'];
        $aError[404] = ['key'=>'api_not_found', 'default'=>'API not found'];
        $aError[900] = ['key'=>'generic_error', 'default'=>'Sorry, unknown Error'];
        
        return ($bTranslate) ? $this->translate($aError[$iErrorCode]['key']) : $aError[$iErrorCode]['default'];
    }
    
    public function status($bValue=null)
    {
        return (is_null($bValue)) ?  $this->aApiResponse['status'] : $this->aApiResponse['status'] = $bValue;
    }
    
    public function error($iValue=null)
    {
        return (is_null($iValue)) ?  $this->aApiResponse['error'] : $this->aApiResponse['error'] = $iValue;
    }
    
    public function msg($sValue=null)
    {
        return (is_null($sValue)) ?  $this->aApiResponse['msg'] : $this->aApiResponse['msg'] = $sValue;
    }
    
    public function total($iValue=null)
    {
        if(is_null($iValue)) {
            return (isset($this->aApiResponse['total'])) ? $this->aApiResponse['total'] : false;
        }
        
        if(!is_int($iValue)) {
            return false;
        }
        return ($this->aApiResponse['total'] = $iValue);
    }
    
    public function found($iValue=null)
    {
        return (is_null($iValue) && isset($this->aApiResponse['found'])) ?  $this->aApiResponse['found'] : $this->aApiResponse['found'] = $iValue;
    }

    public function prev($sValue=null)
    {
        return (is_null($sValue) && isset($this->aApiResponse['prev'])) ?  $this->aApiResponse['prev'] : $this->aApiResponse['prev'] = $sValue;
    }
    
    public function next($sValue=null)
    {
        return (is_null($sValue) && isset($this->aApiResponse['next'])) ?  $this->aApiResponse['next'] : $this->aApiResponse['next'] = $sValue;
    }
    
    public function translate($sData)
    {
        //Process translation in the current language
        return $sData;
    }
    
  
    
    public function result($mData=null)
    {
        if (is_null($mData)) return (isset($this->aApiResponse['result'])) ? $this->aApiResponse['result'] : null;
        
        $this->aApiResponse['result'] = $mData;
    }
    
    public function output()
    {
        $response = new JsonResponse( $this->aApiResponse );
        $response->setEncodingOptions( $response->getEncodingOptions() | JSON_PRETTY_PRINT );
        return $response;
    }
}
