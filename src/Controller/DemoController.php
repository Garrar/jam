<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DemoController extends Controller
{
    public function __construct(){
        
    }
    
    public function html()
    {
        $number = 90;
        return $this->render('demo/panel.html.twig', array(
            'number' => $number,
        ));
        
        return new Response(
            '<html><body>
<table></table>
</body></html>'
            );
    }
}