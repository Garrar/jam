<?php
// tests/Controller/ApiControllerTest.php
namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiControllerTest extends WebTestCase
{
    private $iSender = 102;
    private $iReceiver = 0;
    private $iId = 0;
    private $aData = [];
    public function __construct()
    {
        //Create some invitations
        $i = 0;
        while ($i<10) {
            $this->send();
            $i++;
        }
        
        //List what we sent before
        $aList = $this->listBySender();
        
        //Try to cancel 2 random invitations
        $aRandKeys = array_rand($aList['result'], 2);
        $this->cancel($aList['result'][$aRandKeys[0]]['id']);
        $this->cancel($aList['result'][$aRandKeys[1]]['id']);
        
        //List invitations by random receiver
        $aList = $this->listByReceiver();
        
        //Try to cancel&decline 2 random invitations
        $aRandKeys = array_rand($aList['result'], 2);
        $this->accept($aList['result'][$aRandKeys[0]]['id'], $aList['result'][$aRandKeys[0]]['receiver']);
        $this->decline($aList['result'][$aRandKeys[1]]['id'], $aList['result'][$aRandKeys[0]]['receiver']);
        
        //Try to manage invitation by other receivers
        echo "# Try to manage invitation by diff. receivers".PHP_EOL;
        echo "########".PHP_EOL;
        
        $this->accept($aList['result'][$aRandKeys[0]]['id'], $aList['result'][$aRandKeys[0]]['receiver']+1);
        $this->decline($aList['result'][$aRandKeys[1]]['id'], $aList['result'][$aRandKeys[0]]['receiver']+1);
        
        $this->cancel(99999);
        $this->accept(99999,$aList['result'][$aRandKeys[0]]['receiver']);
        $this->decline(99999,$aList['result'][$aRandKeys[0]]['receiver']);
    }
    
    private function receiver()
    {
        $aItem = [10,20,30];
        $iKey = array_rand($aItem);
        return $aItem[$iKey];
    }
    
    private function subject()
    {
        $aItem = ['Kickoff Meeting'
            ,'Standup Meeting'
            ,'rendez-vous'
            ,'About company'
            ,'HR - Employee & Safty'
        ];
        $iKey = array_rand($aItem);
        return $aItem[$iKey];
    }
    
    private function desc()
    {
        $aItem = ['Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.'
            ,'Sed ultricies, ligula et fermentum feugiat, neque ex egestas odio, ut scelerisque lorem lorem quis sapien. Praesent lobortis nunc in faucibus interdum'
            ,'Suspendisse vehicula nunc lectus, ac finibus massa rutrum nec. Vivamus et iaculis neque. Nam a rhoncus mauris. Sed ante nisl, viverra quis sagittis ut, tincidunt vel sapien. Vivamus tristique arcu vel nunc fermentum malesuada. Etiam dolor justo, mollis eu urna id, efficitur lacinia odio. Curabitur ultrices fermentum nisl, eu euismod lorem commodo eu. Vestibulum a ante tincidunt, iaculis turpis non, imperdiet purus. Quisque elementum at magna sed efficitur.'
            ,'Nam lacinia pulvinar suscipit. Morbi volutpat augue nec tellus hendrerit, sit amet dictum erat fringilla. Nulla pharetra malesuada varius.'
            ,' Aliquam rhoncus nunc a sem dictum rhoncus. Curabitur quis vestibulum lectus. Cras ac maximus sem. Vivamus ac lectus ac velit posuere dictum.'
        ];
        $iKey = array_rand($aItem);
        return $aItem[$iKey];
    }
    
    public function send()
    {
        $this->iReceiver = $this->receiver();
        
        $aParams = ['subject'=>$this->subject()
            , 'description'=>$this->desc()
            , 'sender'=>$this->iSender
            , 'receiver'=>$this->iReceiver
//             , 'end_date'=>'2018-04-26 00:00:00'
            , 'start_date'=>'2018-04-25 00:00:00'
            ,'location'=>'Meeting Room ('.rand(1,10).'), Berlin'];
        
        $sResponse = $this->get_curl('http://localhost:8000/invitation/send', $aParams);
//          echo $sResponse;
        
        $oResponse = json_decode($sResponse);
        
        if ($oResponse->status) {
            $this->iId = $oResponse->result->id;
        }
        
        unset($oResponse->result);
        echo json_encode($oResponse).PHP_EOL;
    }
    
    public function listBySender()
    {
        $sRv = $this->get_curl('http://localhost:8000/invitation/list', ['sender'=>$this->iSender]);
        return json_decode($sRv, 1);
    }
    
    public function listByReceiver()
    {
        $sRv = $this->get_curl('http://localhost:8000/invitation/list', ['receiver'=>$this->receiver()]);
        return json_decode($sRv, 1);
    }
    
    public function cancel($iId)
    {
        $this->get_curl('http://localhost:8000/invitation/'.__FUNCTION__, ['id'=>$iId]);
    }
    
    public function accept($iId,$iReceiver)
    {
        $this->get_curl('http://localhost:8000/invitation/'.__FUNCTION__, ['id'=>$iId, 'receiver'=>$iReceiver]);
    }
    
    public function decline($iId,$iReceiver)
    {
        $this->get_curl('http://localhost:8000/invitation/'.__FUNCTION__, ['id'=>$iId, 'receiver'=>$iReceiver]);
    }
    
    public function testShowPost()
    {
//         echo $this->get_curl('http://localhost:8000/invitation/get', ['id'=>2]);
    }
    
    public function get_curl($sUrl, $aParams) {
        echo PHP_EOL.'################'.PHP_EOL;
        echo "# Run: $sUrl - " . json_encode($aParams). PHP_EOL;
        echo '################'.PHP_EOL;
        set_time_limit ( 0 );
        $curl = curl_init ();
        if (! empty ( $aParams )) {
            curl_setopt ( $curl, CURLOPT_POST, true );
            curl_setopt ( $curl, CURLOPT_POSTFIELDS, http_build_query ( $aParams ) );
        } else {
            print_r ( $aParams );
        }
        curl_setopt ( $curl, CURLOPT_HEADER, false );
        curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt ( $curl, CURLOPT_URL, $sUrl );
        curl_setopt ( $curl, CURLOPT_CONNECTTIMEOUT, 0 );
        curl_setopt ( $curl, CURLOPT_TIMEOUT, 400 ); // timeout in seconds
        
        $response = curl_exec ( $curl );
        curl_close ( $curl );
        
        echo $response.PHP_EOL;
        
        return $response;
    }
}