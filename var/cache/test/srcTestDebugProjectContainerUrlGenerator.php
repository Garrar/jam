<?php

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Psr\Log\LoggerInterface;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcTestDebugProjectContainerUrlGenerator extends Symfony\Component\Routing\Generator\UrlGenerator
{
    private static $declaredRoutes;

    public function __construct(RequestContext $context, LoggerInterface $logger = null)
    {
        $this->context = $context;
        $this->logger = $logger;
        if (null === self::$declaredRoutes) {
            self::$declaredRoutes = array(
        'app_jam_demo' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'App\\Controller\\DemoController::html',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/demo/',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'app_jam_post' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'App\\Controller\\InvitationsController::send',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/invitation/send',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'app_jam_get' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'App\\Controller\\InvitationsController::getItem',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/invitation/get',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'app_jam_list' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'App\\Controller\\InvitationsController::list',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/invitation/list',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'app_jam_delete' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'App\\Controller\\InvitationsController::delete',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/invitation/delete',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'app_jam_cancel' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'App\\Controller\\InvitationsController::cancel',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/invitation/cancel',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'app_jam_accept_invitation' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'App\\Controller\\InvitationsController::accept',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/invitation/accept',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'app_jam_decline_invitation' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'App\\Controller\\InvitationsController::decline',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/invitation/decline',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'app_jam_list_invitions' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'App\\Controller\\InvitationsController::list_sender_invitations',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/invitation/list_by_sender',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'app_jam_list_invited_invitations' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'App\\Controller\\InvitationsController::list_invited_invitations',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/invitation/list_by_invited',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
    );
        }
    }

    public function generate($name, $parameters = array(), $referenceType = self::ABSOLUTE_PATH)
    {
        if (!isset(self::$declaredRoutes[$name])) {
            throw new RouteNotFoundException(sprintf('Unable to generate a URL for the named route "%s" as such route does not exist.', $name));
        }

        list($variables, $defaults, $requirements, $tokens, $hostTokens, $requiredSchemes) = self::$declaredRoutes[$name];

        return $this->doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $referenceType, $hostTokens, $requiredSchemes);
    }
}
