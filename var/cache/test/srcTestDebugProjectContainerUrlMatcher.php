<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcTestDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($rawPathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request ?: $this->createRequest($pathinfo);
        $requestMethod = $canonicalMethod = $context->getMethod();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }

        // app_jam_demo
        if ('/demo' === $trimmedPathinfo) {
            $ret = array (  '_controller' => 'App\\Controller\\DemoController::html',  '_route' => 'app_jam_demo',);
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif ('GET' !== $canonicalMethod) {
                goto not_app_jam_demo;
            } else {
                return array_replace($ret, $this->redirect($rawPathinfo.'/', 'app_jam_demo'));
            }

            return $ret;
        }
        not_app_jam_demo:

        if (0 === strpos($pathinfo, '/invitation')) {
            // app_jam_post
            if ('/invitation/send' === $pathinfo) {
                return array (  '_controller' => 'App\\Controller\\InvitationsController::send',  '_route' => 'app_jam_post',);
            }

            // app_jam_get
            if ('/invitation/get' === $pathinfo) {
                return array (  '_controller' => 'App\\Controller\\InvitationsController::getItem',  '_route' => 'app_jam_get',);
            }

            if (0 === strpos($pathinfo, '/invitation/list')) {
                // app_jam_list
                if ('/invitation/list' === $pathinfo) {
                    return array (  '_controller' => 'App\\Controller\\InvitationsController::list',  '_route' => 'app_jam_list',);
                }

                // app_jam_list_invitions
                if ('/invitation/list_by_sender' === $pathinfo) {
                    return array (  '_controller' => 'App\\Controller\\InvitationsController::list_sender_invitations',  '_route' => 'app_jam_list_invitions',);
                }

                // app_jam_list_invited_invitations
                if ('/invitation/list_by_invited' === $pathinfo) {
                    return array (  '_controller' => 'App\\Controller\\InvitationsController::list_invited_invitations',  '_route' => 'app_jam_list_invited_invitations',);
                }

            }

            // app_jam_delete
            if ('/invitation/delete' === $pathinfo) {
                return array (  '_controller' => 'App\\Controller\\InvitationsController::delete',  '_route' => 'app_jam_delete',);
            }

            // app_jam_decline_invitation
            if ('/invitation/decline' === $pathinfo) {
                return array (  '_controller' => 'App\\Controller\\InvitationsController::decline',  '_route' => 'app_jam_decline_invitation',);
            }

            // app_jam_cancel
            if ('/invitation/cancel' === $pathinfo) {
                return array (  '_controller' => 'App\\Controller\\InvitationsController::cancel',  '_route' => 'app_jam_cancel',);
            }

            // app_jam_accept_invitation
            if ('/invitation/accept' === $pathinfo) {
                return array (  '_controller' => 'App\\Controller\\InvitationsController::accept',  '_route' => 'app_jam_accept_invitation',);
            }

        }

        if ('/' === $pathinfo && !$allow) {
            throw new Symfony\Component\Routing\Exception\NoConfigurationException();
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
