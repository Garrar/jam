<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($rawPathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request ?: $this->createRequest($pathinfo);
        $requestMethod = $canonicalMethod = $context->getMethod();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }

        if (0 === strpos($pathinfo, '/_')) {
            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if ('/_profiler' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif ('GET' !== $canonicalMethod) {
                        goto not__profiler_home;
                    } else {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', '_profiler_home'));
                    }

                    return $ret;
                }
                not__profiler_home:

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ('/_profiler/search' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ('/_profiler/search_bar' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_phpinfo
                if ('/_profiler/phpinfo' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler_open_file
                if ('/_profiler/open' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:openAction',  '_route' => '_profiler_open_file',);
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

        }

        // app_jam_demo
        if ('/demo' === $trimmedPathinfo) {
            $ret = array (  '_controller' => 'App\\Controller\\DemoController::html',  '_route' => 'app_jam_demo',);
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif ('GET' !== $canonicalMethod) {
                goto not_app_jam_demo;
            } else {
                return array_replace($ret, $this->redirect($rawPathinfo.'/', 'app_jam_demo'));
            }

            return $ret;
        }
        not_app_jam_demo:

        if (0 === strpos($pathinfo, '/invitation')) {
            // app_jam_post
            if ('/invitation/send' === $pathinfo) {
                return array (  '_controller' => 'App\\Controller\\InvitationsController::send',  '_route' => 'app_jam_post',);
            }

            // app_jam_get
            if ('/invitation/get' === $pathinfo) {
                return array (  '_controller' => 'App\\Controller\\InvitationsController::getItem',  '_route' => 'app_jam_get',);
            }

            if (0 === strpos($pathinfo, '/invitation/list')) {
                // app_jam_list
                if ('/invitation/list' === $pathinfo) {
                    return array (  '_controller' => 'App\\Controller\\InvitationsController::list',  '_route' => 'app_jam_list',);
                }

                // app_jam_list_invitions
                if ('/invitation/list_by_sender' === $pathinfo) {
                    return array (  '_controller' => 'App\\Controller\\InvitationsController::list_sender_invitations',  '_route' => 'app_jam_list_invitions',);
                }

                // app_jam_list_invited_invitations
                if ('/invitation/list_by_invited' === $pathinfo) {
                    return array (  '_controller' => 'App\\Controller\\InvitationsController::list_invited_invitations',  '_route' => 'app_jam_list_invited_invitations',);
                }

            }

            // app_jam_delete
            if ('/invitation/delete' === $pathinfo) {
                return array (  '_controller' => 'App\\Controller\\InvitationsController::delete',  '_route' => 'app_jam_delete',);
            }

            // app_jam_decline_invitation
            if ('/invitation/decline' === $pathinfo) {
                return array (  '_controller' => 'App\\Controller\\InvitationsController::decline',  '_route' => 'app_jam_decline_invitation',);
            }

            // app_jam_cancel
            if ('/invitation/cancel' === $pathinfo) {
                return array (  '_controller' => 'App\\Controller\\InvitationsController::cancel',  '_route' => 'app_jam_cancel',);
            }

            // app_jam_accept_invitation
            if ('/invitation/accept' === $pathinfo) {
                return array (  '_controller' => 'App\\Controller\\InvitationsController::accept',  '_route' => 'app_jam_accept_invitation',);
            }

        }

        if ('/' === $pathinfo && !$allow) {
            throw new Symfony\Component\Routing\Exception\NoConfigurationException();
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
